package com.marinshalamanov.geodrawer.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

import com.marinshalamanov.geodrawer.R;

public class WelcomeActivity extends Activity {

	private final static int TIMEOUT_MS = 2000;

	private final Class<? extends Activity> nextActivity = MainActivity.class;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);

		final Context context = this;
		Thread splashThread = new Thread() {
			@Override
			public void run() {
				try {
					sleep(TIMEOUT_MS);
				} catch (InterruptedException e) {
					// do nothing
				} finally {
					finish();
					Intent intent = new Intent(context, nextActivity);
					startActivity(intent);
				}
			}
		};
		splashThread.start();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.welcome, menu);
		return true;
	}

}
