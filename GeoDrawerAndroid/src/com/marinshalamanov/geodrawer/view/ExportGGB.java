package com.marinshalamanov.geodrawer.view;

import georegression.struct.line.LineSegment2D_F32;
import georegression.struct.point.Point2D_F32;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import android.content.Context;
import android.os.Environment;

import com.marinshalamanov.geodrawer.model.Diagram;

public class ExportGGB {

	private static final float EPSILON = (float) 1e-5;

	public void export(Diagram diagram, String resultFilePath, Context context) {

		try {
			String dirname = Environment.getExternalStorageDirectory().getPath() + "/export";
//			String dirname = "export";
			File dir = new File(dirname);
			dir.mkdirs();
			
			String xml = getXML(diagram);
			PrintWriter writer;
			File geogebraXML = new File(dirname + "/geogebra.xml");
			if (geogebraXML.exists()) {
				geogebraXML.delete();
			}
			geogebraXML.createNewFile();

			FileOutputStream openFileOutput;
			openFileOutput = new FileOutputStream(geogebraXML);
//			openFileOutput = context.openFileOutput(geogebraXML.getName(), Context.MODE_PRIVATE);
			writer = new PrintWriter(openFileOutput);
			writer.println(xml);
			writer.close();
			openFileOutput.close();
			
			File javascript = new File(dirname + "/geogebra_javascript.js");
			if (javascript.exists()) {
				javascript.delete();
			}
			javascript.createNewFile();

			openFileOutput = new FileOutputStream(javascript);
//			openFileOutput = context.openFileOutput(javascript.getName(), Context.MODE_PRIVATE);
			writer = new PrintWriter(openFileOutput);
			String js = getJS();
			writer.println(js);
			writer.close();
			openFileOutput.close();
			
//			File result = new File(resultFilePath);
//			if(result.exists()) {
//				result.delete();
//			}
			
			AppZip appZip = new AppZip();
			appZip.SOURCE_FOLDER = dir.getAbsolutePath();
			appZip.generateFileList(dir);
			appZip.zipIt(resultFilePath, context);
			
			javascript.delete();
			geogebraXML.delete();
			dir.delete();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private String getPointName(int pointIndex) {
		return "" + (char) ('A' + pointIndex);
	}

	private String getLineName(int lineIndex) {
		return "" + (char) ('a' + lineIndex);
	}

	private int getPointIndex(Point2D_F32 point, Diagram diagram) {
		List<Point2D_F32> points = diagram.getPoints();
		for (int i = 0; i < points.size(); i++) {
			if (point.isIdentical(points.get(i), EPSILON)) {
				return i;
			}
		}

		return -1;
	}

	private String getJS() {
		return "function ggbOnInit() {}";
	}

	private String getXML(Diagram diagram) {
		String xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><geogebra format=\"4.2\" version=\"4.2.60.0\" id=\"8620c2b4-e657-4339-a875-6a75509ec0b9\"  xsi:noNamespaceSchemaLocation=\"http://www.geogebra.org/ggb.xsd\" xmlns=\"\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ><gui>	<window width=\"800\" height=\"600\" />	<perspectives><perspective id=\"tmp\">	<panes>		<pane location=\"\" divider=\"0.15331278890600925\" orientation=\"1\" />	</panes>	<views>		<view id=\"4097\" visible=\"false\" inframe=\"true\" stylebar=\"true\" location=\"1,1,1\" size=\"400\" window=\"100,100,700,550\" />		<view id=\"4\" toolbar=\"0 59 || 2020 , 2021 , 2022 , 66 || 2001 , 2003 , 2002 , 2004 , 2005 || 2040 , 2041 , 2042 , 2044 , 2043\" visible=\"false\" inframe=\"false\" stylebar=\"true\" location=\"1,1\" size=\"300\" window=\"100,100,600,400\" />		<view id=\"8\" toolbar=\"1001 | 1002 | 1003  || 1005 | 1004 || 1006 | 1007 | 1010 | 1011 || 1008 1009 || 6\" visible=\"false\" inframe=\"false\" stylebar=\"true\" location=\"1,3\" size=\"300\" window=\"100,100,600,400\" />		<view id=\"1\" visible=\"true\" inframe=\"false\" stylebar=\"false\" location=\"1\" size=\"1094\" window=\"100,100,600,400\" />		<view id=\"2\" visible=\"true\" inframe=\"false\" stylebar=\"false\" location=\"3\" size=\"199\" window=\"100,100,250,400\" />		<view id=\"16\" visible=\"false\" inframe=\"false\" stylebar=\"false\" location=\"1\" size=\"150\" window=\"50,50,500,500\" />		<view id=\"32\" visible=\"false\" inframe=\"false\" stylebar=\"true\" location=\"1\" size=\"150\" window=\"50,50,500,500\" />		<view id=\"64\" visible=\"false\" inframe=\"true\" stylebar=\"true\" location=\"1\" size=\"150\" window=\"50,50,500,500\" />		<view id=\"70\" toolbar=\"0 || 2020 || 2021 || 2022\" visible=\"false\" inframe=\"true\" stylebar=\"true\" location=\"1\" size=\"150\" window=\"50,50,500,500\" />	</views>	<toolbar show=\"true\" items=\"0 39 59 | 1 501 67 , 5 19 , 72 | 2 15 45 , 18 65 , 7 37 | 4 3 8 9 , 13 44 , 58 , 47 | 16 51 64 , 70 | 10 34 53 11 , 24  20 22 , 21 23 | 55 56 57 , 12 | 36 46 , 38 49 50 , 71 | 30 29 54 32 31 33 | 17 26 62 73 , 14 66 68 | 25 52 60 61 | 40 41 42 , 27 28 35 , 6\" position=\"1\" help=\"true\" />	<input show=\"true\" cmd=\"true\" top=\"false\" />	<dockBar show=\"true\" east=\"true\" /></perspective>	</perspectives>	<labelingStyle  val=\"3\"/>	<font  size=\"12\"/>	<graphicsSettings javaLatexFonts=\"false\"/></gui><euclidianView>	<size  width=\"1094\" height=\"543\"/>	<coordSystem xZero=\"215.0\" yZero=\"315.0\" scale=\"49.99999999999999\" yscale=\"50.0\"/>	<evSettings axes=\"true\" grid=\"false\" gridIsBold=\"false\" pointCapturing=\"3\" rightAngleStyle=\"1\" checkboxSize=\"13\" gridType=\"0\"/>	<bgColor r=\"255\" g=\"255\" b=\"255\"/>	<axesColor r=\"0\" g=\"0\" b=\"0\"/>	<gridColor r=\"192\" g=\"192\" b=\"192\"/>	<lineStyle axes=\"1\" grid=\"10\"/>	<axis id=\"0\" show=\"true\" label=\"\" unitLabel=\"\" tickStyle=\"1\" showNumbers=\"true\"/>	<axis id=\"1\" show=\"true\" label=\"\" unitLabel=\"\" tickStyle=\"1\" showNumbers=\"true\"/></euclidianView><kernel>	<continuous val=\"false\"/>	<usePathAndRegionParameters val=\"true\"/>	<decimals val=\"2\"/>	<angleUnit val=\"degree\"/>	<algebraStyle val=\"0\"/>	<coordStyle val=\"0\"/>	<angleFromInvTrig val=\"false\"/></kernel><scripting blocked=\"false\" disabled=\"false\"/><construction title=\"\" author=\"\" date=\"\">";

		String pointElementTemplate = "<element type=\"point\" label=\"%s\">	<show object=\"true\" label=\"true\"/>	<objColor r=\"%d\" g=\"%d\" b=\"%d\" alpha=\"0.0\"/>	<layer val=\"0\"/>	<labelMode val=\"0\"/>	<animation step=\"1\" speed=\"1\" type=\"1\" playing=\"false\"/>	<coords x=\"%f\" y=\"%f\" z=\"%f\"/>	<pointSize val=\"3\"/>	<pointStyle val=\"0\"/></element>";
		String lineElementTemplate = "<command name=\"Segment\">	<input a0=\"%s\" a1=\"%s\"/>	<output a0=\"%s\"/></command><element type=\"segment\" label=\"%s\">	<show object=\"true\" label=\"false\"/>	<objColor r=\"%d\" g=\"%d\" b=\"%d\" alpha=\"0.0\"/>	<layer val=\"0\"/>	<labelMode val=\"0\"/>	<coords x=\"%f\" y=\"%f\" z=\"%f\"/>	<lineStyle thickness=\"2\" type=\"0\" typeHidden=\"1\"/>	<outlyingIntersections val=\"false\"/>	<keepTypeOnTransform val=\"true\"/></element>";

		List<Point2D_F32> points = diagram.getPoints();
		for (int i = 0; i < points.size(); i++) {
			Point2D_F32 point = points.get(i);
			String label = getPointName(i);
			int colorR = 0;
			int colorG = 0;
			int colorB = 256;

			double positionX = point.getX();
			double positionY = point.getY();
			double positionZ = 1.0;

			String element = String.format(pointElementTemplate, label, colorR,
					colorG, colorB, positionX, positionY, positionZ);
			xml += element;
		}

		List<LineSegment2D_F32> lines = diagram.getLines();

		for (int i = 0; i < lines.size(); i++) {
			LineSegment2D_F32 line = lines.get(i);
			String pointA = getPointName(getPointIndex(line.getA(), diagram));
			String pointB = getPointName(getPointIndex(line.getB(), diagram));
			String lineLabel = getLineName(i);

			int colorR = 0;
			int colorG = 0;
			int colorB = 0;

			double positionX = line.getA().y - line.getB().y;
			double positionY = -(line.getA().x - line.getB().x);
			double positionZ = 1.0;

			String element = String.format(lineElementTemplate, pointA, pointB, lineLabel,
					lineLabel, colorR, colorG, colorB, positionX, positionY,
					positionZ);
			xml += element;
		}

		xml += "</construction></geogebra>";
		return xml;
	}
	
	public static void main(String[] args) {
		Diagram diagram = new Diagram();
		Point2D_F32 pointA = new Point2D_F32(0, 0);
		Point2D_F32 pointB = new Point2D_F32(5, 5);
		diagram.addPoint(pointA);
		diagram.addPoint(pointB);
		diagram.addLine(new LineSegment2D_F32(pointA, pointB));
		new ExportGGB().export(diagram, "C:\\Users\\user\\Desktop\\export.ggb", null);
	}
	
}
