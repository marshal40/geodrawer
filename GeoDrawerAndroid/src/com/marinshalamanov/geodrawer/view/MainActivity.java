package com.marinshalamanov.geodrawer.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import boofcv.android.ConvertBitmap;
import boofcv.struct.image.ImageFloat32;

import com.marinshalamanov.geodrawer.DiagramParser;
import com.marinshalamanov.geodrawer.R;
import com.marinshalamanov.geodrawer.model.Diagram;
import com.marinshalamanov.geodrawer.pointextractor.PointExtractor;
import com.marinshalamanov.geodrawer.pointextractor.SimpleClusteringPointExtractor;
import com.marinshalamanov.geodrawer.segmentextractor.SmartSegmentExtractor;
import com.marinshalamanov.geodrawer.thersholder.SectionThresholder;
import com.marinshalamanov.geodrawer.touch.TouchMode;
import com.marinshalamanov.geodrawer.view.colorpicker.ColorPickerDialog;
import com.marinshalamanov.geodrawer.view.colorpicker.OnColorSelectedListener;
import com.marinshalamanov.geodrawer.view.openfiledialog.OnFileSelectedListener;
import com.marinshalamanov.geodrawer.view.openfiledialog.OpenFileDialog;
import com.marinshalamanov.geodrawer.view.photo.AlbumStorageDirFactory;
import com.marinshalamanov.geodrawer.view.photo.BaseAlbumDirFactory;
import com.marinshalamanov.geodrawer.view.photo.FroyoAlbumDirFactory;

public class MainActivity extends FragmentActivity {

	private static final int ACTION_TAKE_PHOTO = 0x1;

	private static final int ACTION_PICK_PHOTO = 0x2;

	private DrawingCanvas canvas;

	private AlbumStorageDirFactory albumStorageDirFactory = null;

	private String currentPhotoPath;

	private ImageView mImageView = null;

	private File photoFile = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mImageView = (ImageView) findViewById(R.id.imageView);

		canvas = (DrawingCanvas) findViewById(R.id.canvas);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
			albumStorageDirFactory = new FroyoAlbumDirFactory();
		} else {
			albumStorageDirFactory = new BaseAlbumDirFactory();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		boolean endOfBackStack = !canvas.undo();
		if (endOfBackStack) {
			super.onBackPressed();
		}
	}

	public void onBackButton(View v) {
		canvas.undo();
	}

	public void onClearButton(View v) {
		canvas.init();
	}

	public void onDrawButton(View v) {
		canvas.setMode(TouchMode.DRAW);
	}

	public void onPaintButton(View v) {
		canvas.setMode(TouchMode.PAINT);
	}

	public void onOpenButton(View v) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		OpenFileDialog openFileDialog = new OpenFileDialog();
		openFileDialog.init(this, new OnFileSelectedListener() {

			@Override
			public void onFileSelected(String filename) {
				open(filename);
			}
		});
		openFileDialog.show(fragmentManager, "OpenFileDialog");
	}

	public void onSaveButton(View v) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.writeFilenameMessage);

		// Set up the input
		final EditText input = new EditText(this);
		// Specify the type of input expected; this, for example, sets the input
		// as a password, and will mask the text
		builder.setView(input);

		// Set up the buttons
		builder.setPositiveButton(R.string.save_button,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						String filename = input.getText().toString();
						save(filename);
					}
				});
		builder.setNegativeButton(R.string.cancelButton,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		builder.show();
	}

	public File[] getAllSaves() {
		String filepath = getSaveDirPath();
		File dir = new File(filepath);
		File[] listFiles = dir.listFiles();
		return listFiles;
	}

	private String getSaveDirPath() {
		String sdcard = Environment.getExternalStorageDirectory().getPath();
		String folderName = getString(R.string.app_name);

		String filepath = sdcard + "/" + folderName;
		return filepath;
	}
	
	private String getExportDirPath() {
		String sdcard = Environment.getExternalStorageDirectory().getPath();
		String folderName = getString(R.string.app_name) + "Export";

		String filepath = sdcard + "/" + folderName;
		return filepath;
	}

	private void save(String filename) {
		String dirPath = getSaveDirPath();

		String filepath = dirPath + "/" + filename;
		Log.d("Saving", "File: " + filepath);

		File file = new File(filepath);

		Log.e("1", "3");
		if (file.exists()) {

			String message = getString(R.string.file_exists);
			Log.d("Saving", message);
			// TODO: show alert

			file.delete();
		}

		Log.e("1", "4");
		try {
			file.getParentFile().mkdirs();
			file.createNewFile();
		} catch (IOException e) {
			Log.e("save diagram", "file: " + filepath, e);
		}

		Log.e("1", "4");
		FileOutputStream writer = null;
		try {
			writer = openFileOutput(file.getName(), Context.MODE_PRIVATE);
		} catch (FileNotFoundException e) {
			Log.e("save diagram", "file: " + filepath, e);
		}

		Log.e("1", "5");

		Diagram diagram = canvas.getDiagram();
		String json = diagram.toJSON();

		try {
			Log.e("saving", new String(json.getBytes()));
			writer.write(json.getBytes());
			writer.flush();
			writer.close();
		} catch (IOException e) {
			Log.e("save diagram", "file: " + filepath, e);
		}
	}

	private void open(String filename) {
		String sdcard = Environment.getExternalStorageDirectory().getPath();
		String folderName = getString(R.string.app_name);

		String filepath = sdcard + "/" + folderName + "/" + filename;
		Log.d("Opening", "File: " + filepath);

		File file = new File(filepath);

		if (!file.exists()) {
			Log.e("Opening", "File does not exist");
			// TODO: show alert

			return;
		}

		Diagram diagram = canvas.getDiagram();

		String json = new String();

		try {
			FileInputStream fis = openFileInput(file.getName());
			byte[] buffer = new byte[(int) fis.getChannel().size()];
			fis.read(buffer);
			for (byte b : buffer) {
				json += (char) b;
			}
			fis.close();
			Log.i("Opening", String.format("GOT: [%s]", json));
		} catch (IOException e) {
			Log.e("Opening", e.getMessage(), e);
			return;
		}

		diagram.set(Diagram.createFromJSON(json));
	}

	public void onColorButton(View v) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		ColorPickerDialog colorPickerDialog = new ColorPickerDialog();
		colorPickerDialog
				.setOnColorSelectedListener(new OnColorSelectedListener() {
					@Override
					public void onColorSelected(int colorHex) {
						canvas.setDrawingColor(colorHex);
					}
				});
		colorPickerDialog.show(fragmentManager, "ColorPickerDialog");
	}

	@SuppressLint("SdCardPath")
	public void takePhoto(View v) {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		photoFile = null;

		// currentPhotoPath =
		// "/mnt/sdcard/Pictures/GeoDrawer/1391698793457_-1600118833.jpg";
		// setPic();

		try {
			// Create an image file name
			String imageFileName = System.currentTimeMillis() + "_";
			File albumF = getAlbumDir();
			photoFile = File.createTempFile(imageFileName, ".jpg", albumF);

			currentPhotoPath = photoFile.getAbsolutePath();
			Log.e("path build", currentPhotoPath);
			takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
					Uri.fromFile(photoFile));
		} catch (IOException e) {
			Log.e("take photo()", "io exc", e);
			photoFile = null;
			currentPhotoPath = null;
		}

		startActivityForResult(takePictureIntent, ACTION_TAKE_PHOTO);
	}

	public void pickPhoto(View v) {
		Intent takePictureIntent = new Intent(Intent.ACTION_PICK);
		takePictureIntent.setType("image/*");

		startActivityForResult(takePictureIntent, ACTION_PICK_PHOTO);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		Log.d("onActivityResult", requestCode + " " + resultCode + " "
				+ currentPhotoPath);

		if (resultCode == RESULT_OK) {

			currentPhotoPath = data.getDataString();

			switch (requestCode) {
			case ACTION_TAKE_PHOTO: {
				if (currentPhotoPath != null) {
					setPic();
					Log.d("setPic", "finnished");
					galleryAddPic();
					Log.d("galleryAddPic", "finished");
					currentPhotoPath = null;
				}

				break;
			}
			case ACTION_PICK_PHOTO: {
				Uri selectedImage = data.getData();

				String[] filePathColumn = { MediaStore.Images.Media.DATA };

				Cursor cursor = getContentResolver().query(selectedImage,
						filePathColumn, null, null, null);
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String filePath = cursor.getString(columnIndex);
				cursor.close();

				currentPhotoPath = filePath;
				setPic();
				Log.d("setPic", "finnished");
				galleryAddPic();
				Log.d("galleryAddPic", "finished");
				currentPhotoPath = null;

				break;
			}
			}
		}
	}

	private File getAlbumDir() {
		File storageDir = null;

		if (Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())) {

			storageDir = albumStorageDirFactory
					.getAlbumStorageDir(getString(R.string.app_name));

			if (storageDir != null) {
				if (!storageDir.mkdirs()) {
					if (!storageDir.exists()) {
						Log.d("CameraSample", "failed to create directory");
						return null;
					}
				}
			}

		} else {
			Log.v(getString(R.string.app_name),
					"External storage is not mounted READ/WRITE.");
		}

		return storageDir;
	}

	private void setPic() {

		/* There isn't enough memory to open up more than a couple camera photos */
		/* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */

		SectionThresholder thresholder = new SectionThresholder();
		SmartSegmentExtractor lineExtractor = new SmartSegmentExtractor();
		PointExtractor pointExtractor = new SimpleClusteringPointExtractor();
		DiagramParser diagramParser = new DiagramParser(thresholder,
				lineExtractor, pointExtractor);

		Log.d("setPic", "beforeParse");

		String prefix = "file://";
		if (currentPhotoPath.startsWith(prefix)) {
			currentPhotoPath = currentPhotoPath.substring(prefix.length());
		}
		Log.e("setPic", currentPhotoPath);

		Bitmap imageBitmap = decodeFile(new File(currentPhotoPath), 600);
		if (imageBitmap == null) {
			Log.e("loadImage", "failed: Loaded bitmap is null");
			return;
		}

		// Associate the Bitmap to the ImageView
		mImageView.setImageBitmap(imageBitmap);
		mImageView.setVisibility(View.VISIBLE);

		ImageFloat32 imageF32 = ConvertBitmap.bitmapToGray(imageBitmap,
				(ImageFloat32) null, null);

		Diagram diagram = null;
		try {
			diagram = diagramParser.parse(imageF32);
		} catch (Exception e) {
			Log.e("setPic", "Exception while parsing.", e);
			return;
		}

		canvas.setDiagram(diagram);
	}

	private Bitmap decodeFile(File f, int requiredSize) {
		try {
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);

			// Find the correct scale value. It should be the power of 2.
			Log.d("decodeFile", "dimensions " + o.outWidth + " " + o.outHeight);

			int scale = 1;
			while (o.outWidth / scale / 2 >= requiredSize
					&& o.outHeight / scale / 2 >= requiredSize) {
				scale *= 2;
			}

			Log.d("decodeFile", "decoding with scale of " + scale);

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {
		}
		return null;
	}

	private void galleryAddPic() {
		Intent mediaScanIntent = new Intent(
				"android.intent.action.MEDIA_SCANNER_SCAN_FILE");
		File f = new File(currentPhotoPath);
		Uri contentUri = Uri.fromFile(f);
		mediaScanIntent.setData(contentUri);
		this.sendBroadcast(mediaScanIntent);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.gallery:
			pickPhoto(null);
			return true;
		case R.id.photo:
			takePhoto(null);
			return true;
		case R.id.open:
			onOpenButton(null);
			return true;
		case R.id.save:
			onSaveButton(null);
			return true;
		case R.id.export:
			Diagram diagram = canvas.getDiagram();
			String resultFilePath = new File(getSaveDirPath(), "export.ggb").getAbsolutePath();
			new ExportGGB().export(diagram, resultFilePath, this);
			Toast.makeText(this, "Изведено в " + resultFilePath, Toast.LENGTH_LONG).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
