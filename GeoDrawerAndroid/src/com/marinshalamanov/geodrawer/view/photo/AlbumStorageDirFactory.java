package com.marinshalamanov.geodrawer.view.photo;

import java.io.File;

abstract public class AlbumStorageDirFactory {
	public abstract File getAlbumStorageDir(String albumName);
}
