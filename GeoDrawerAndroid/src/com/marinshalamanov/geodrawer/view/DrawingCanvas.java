package com.marinshalamanov.geodrawer.view;

import georegression.struct.line.LineSegment2D_F32;
import georegression.struct.point.Point2D_F32;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import com.marinshalamanov.geodrawer.model.Diagram;
import com.marinshalamanov.geodrawer.touch.GDTouchListener;
import com.marinshalamanov.geodrawer.touch.Touch;
import com.marinshalamanov.geodrawer.touch.TouchMode;

public class DrawingCanvas extends View {

	private final Diagram diagram = new Diagram();

	private Paint linePaint;

	private Paint pointPaint;

	private int drawingColor = Color.BLACK;

	private int backgroundColor = Color.WHITE;

	private GDTouchListener touchListener;

	public DrawingCanvas(Context context) {
		super(context);
		init();
	}

	public DrawingCanvas(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public DrawingCanvas(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public int getDrawingColor() {
		return drawingColor;
	}

	public void setDrawingColor(int drawingColor) {
		this.drawingColor = drawingColor;
		touchListener.setDrawingColor(drawingColor);
	}

	public TouchMode getMode() {
		return touchListener.getMode();
	}

	public void setMode(TouchMode mode) {
		touchListener.setMode(mode);
	}

	public void setDiagram(Diagram diagram) {
		Context context = getContext();
		Resources resources = context.getResources();
		DisplayMetrics displayMetrics = resources.getDisplayMetrics();
		int width = displayMetrics.widthPixels;
		int height = displayMetrics.heightPixels;

		float diagramDimensPerc = 0.7f;
		float marginDimens = (1 - diagramDimensPerc) / 2.0f;
		int diagramWidth = (int) (width * diagramDimensPerc);
		int diagramHeight = (int) (height * diagramDimensPerc);
		int marginX = (int) (width * marginDimens);
		int marginY = (int) (height * marginDimens);

		boolean tooSmall = diagramWidth < 20 || diagramHeight < 20;
		if (tooSmall) {
			String message = "diagram is requested to shrink in very small area ("
					+ diagramWidth
					+ " "
					+ diagramHeight
					+ "). No shrink will be performed.";
			Log.e("DrawingCanvas.setDiagram", message);
		} else {
			diagram.shrink(diagramWidth, diagramHeight);
			diagram.offset(marginX, marginY);
		}

		Log.d("Num points", "" + diagram.getPoints().size());
		Log.d("Num lines", "" + diagram.getLines().size());
		List<Point2D_F32> points = diagram.getPoints();
		Log.d("diagram", "points " + points);
		for (LineSegment2D_F32 line : diagram.getLines()) {
			Log.d("diagram", "line: " + line.a + " " + line.b);
		}

		this.diagram.set(diagram);
	}

	public Diagram getDiagram() {
		return diagram;
	}

	public void init() {
		diagram.clear();

		linePaint = new Paint();
		linePaint.setColor(Color.BLACK);
		linePaint.setStrokeWidth(3);

		pointPaint = new Paint();
		pointPaint.setColor(Color.BLACK);
		pointPaint.setStrokeWidth(8);

		touchListener = new GDTouchListener(diagram, this.getContext());
		setOnTouchListener(touchListener);
	}

	@SuppressLint("DrawAllocation")
	@Override
	public void onDraw(Canvas canvas) {
		canvas.drawColor(backgroundColor);

		drawLines(canvas);
		drawPoints(canvas);

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				invalidate();
			}
		}, 30);
	}

	public boolean undo() {
		return touchListener.undo();
	}

	private void drawPoints(Canvas canvas) {
		for (Point2D_F32 point : diagram.getPoints()) {
			canvas.drawCircle(point.getX(), point.getY(), 5, pointPaint);
		}
	}

	private void drawLines(Canvas canvas) {
		for (LineSegment2D_F32 line : diagram.getLines()) {
			Point2D_F32 a = line.getA();
			Point2D_F32 b = line.getB();

			// create the paint
			int currColorHex = diagram.getColor(line);
			Paint currLinePaint = new Paint(linePaint);
			currLinePaint.setColor(currColorHex);

			canvas.drawLine(a.getX(), a.getY(), b.getX(), b.getY(),
					currLinePaint);
		}

		Touch gestureStart = touchListener.getGestureStart();
		Touch gestureEnd = touchListener.getGestureEnd();

		if (gestureStart != null && gestureEnd != null) {
			Point2D_F32 a = gestureStart.getCoords();
			Point2D_F32 b = gestureEnd.getCoords();

			Paint currLinePaint = new Paint(linePaint);
			currLinePaint.setColor(drawingColor);
			canvas.drawLine(a.getX(), a.getY(), b.getX(), b.getY(),
					currLinePaint);
		}
	}
}
