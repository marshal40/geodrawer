package com.marinshalamanov.geodrawer.view.colorpicker;

import java.util.Locale;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;

import com.marinshalamanov.geodrawer.R;

public class ColorPickerDialog extends DialogFragment {

	private OnColorSelectedListener listener = null;

	public void setOnColorSelectedListener(OnColorSelectedListener listener) {
		this.listener = listener;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		FragmentActivity activity = getActivity();
		Resources resources = activity.getResources();
		String chooseColorMsg = resources.getString(R.string.choose_color);

		builder = builder.setTitle(chooseColorMsg);
		builder = builder.setItems(R.array.colors,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (listener == null) {
							return;
						}

						String[] stringArray = getResources().getStringArray(
								R.array.colors);
						String colorSelected = stringArray[which];

						colorSelected = colorSelected.toLowerCase(Locale.US);

						if (colorSelected.equals("�������")) {
							listener.onColorSelected(Color.RED);
						} else if (colorSelected.equals("��������")) {
							listener.onColorSelected(0xFFFFA500);
						} else if (colorSelected.equals("�����")) {
							listener.onColorSelected(Color.YELLOW);
						} else if (colorSelected.equals("������")) {
							listener.onColorSelected(Color.GREEN);
						} else if (colorSelected.equals("�����")) {
							listener.onColorSelected(Color.BLUE);
						} else if (colorSelected.equals("���������")) {
							listener.onColorSelected(0xFF8D38C9);
						} else if (colorSelected.equals("�����")) {
							listener.onColorSelected(Color.BLACK);
						}
					}
				});

		return builder.create();
	}
}
