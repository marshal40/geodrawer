package com.marinshalamanov.geodrawer.view.colorpicker;

public interface OnColorSelectedListener {
	public void onColorSelected(int colorHex);
}
