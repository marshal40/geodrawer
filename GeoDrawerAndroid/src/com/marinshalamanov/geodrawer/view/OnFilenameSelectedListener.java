package com.marinshalamanov.geodrawer.view;

public interface OnFilenameSelectedListener {
	public void onFilenameSelected(String filename);
}
