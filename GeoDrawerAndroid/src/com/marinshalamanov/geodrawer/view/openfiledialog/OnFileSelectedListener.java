package com.marinshalamanov.geodrawer.view.openfiledialog;

public interface OnFileSelectedListener {
	public void onFileSelected(String filename);
}
