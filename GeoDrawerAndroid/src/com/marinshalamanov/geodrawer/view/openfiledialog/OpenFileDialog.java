package com.marinshalamanov.geodrawer.view.openfiledialog;

import java.io.File;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.marinshalamanov.geodrawer.R;
import com.marinshalamanov.geodrawer.view.MainActivity;

public class OpenFileDialog extends DialogFragment {

	private OnFileSelectedListener listener = null;

	private MainActivity activity;

	public void init(MainActivity context, OnFileSelectedListener listener) {
		this.activity = context;
		this.listener = listener;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		File[] allSaves = activity.getAllSaves();
		final String[] savesNames = new String[allSaves.length];
		for (int i = 0; i < allSaves.length; i++) {
			savesNames[i] = allSaves[i].getName();
		}

		Resources resources = activity.getResources();
		String chooseFile = resources.getString(R.string.choose_file);
		builder = builder.setTitle(chooseFile);
		builder = builder.setItems(savesNames,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (listener == null) {
							return;
						}

						String saveName = savesNames[which];
						listener.onFileSelected(saveName);
					}
				});

		return builder.create();
	}
}
