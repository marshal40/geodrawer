package com.marinshalamanov.geodrawer.touch;

import georegression.metric.Distance2D_F32;
import georegression.struct.line.LineSegment2D_F32;
import georegression.struct.point.Point2D_F32;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import android.content.Context;
import android.graphics.Color;
import android.os.Vibrator;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.marinshalamanov.geodrawer.GDUtil;
import com.marinshalamanov.geodrawer.model.Diagram;
import com.marinshalamanov.geodrawer.model.PointOnLineDefinition;

public class GDTouchListener implements OnTouchListener {

	private static final float EPSILON = (float) 1e-5;

	// the maximal movement in the touch to be considered as a single point
	private final static int MAX_DELTA_FOR_POINT = 30;

	// maximal distance between click and line, to be considered on the line
	private final static int MAX_POINT_LINE_DIST = 40;

	private final static int MAX_POINT_CLICK_OFFSET = 50;

	private final static int LONG_CLICK_TIME_MS = 500;

	private Touch gestureStart;

	private Touch gestureEnd;

	private boolean dragMode;

	private Point2D_F32 draggedPoint;

	private List<LineSegment2D_F32> draggedLines;

	private Diagram diagram;

	private Context context;

	private TouchMode mode;

	private int drawingColor = Color.BLACK;

	private Stack<String> backStack = new Stack<String>();

	public GDTouchListener(Diagram diagram, Context context) {
		this.diagram = diagram;
		this.context = context;
		this.mode = TouchMode.DRAW;
	}

	public int getDrawingColor() {
		return drawingColor;
	}

	public void setDrawingColor(int drawingColor) {
		this.drawingColor = drawingColor;
	}

	public Touch getGestureStart() {
		return gestureStart;
	}

	public Touch getGestureEnd() {
		return gestureEnd;
	}

	public TouchMode getMode() {
		return mode;
	}

	public void setMode(TouchMode mode) {
		this.mode = mode;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (mode) {
		case DRAW:
			return onDrawTouch(v, event);
		case PAINT:
			return onPaintTouch(v, event);
		default:
			return onDrawTouch(v, event);
		}
	}

	private boolean isClear() {
		return diagram.getPoints().isEmpty() && diagram.getLines().isEmpty();
	}

	/**
	 * 
	 * @return true if and operation is undone, false if no such operations
	 *         (backstack is empty)
	 */
	public boolean undo() {
		if (!backStack.empty()) {
			String top = backStack.pop();
			Diagram topDiagram = Diagram.createFromJSON(top);
			diagram.set(topDiagram);
			Log.d("touch", "backstack poped. size " + backStack.size());
			return true;
		} else {
			if (isClear()) {
				return false;
			} else {
				diagram.clear();
				return true;
			}
		}
	}

	private void addSnapshotToBackstack() {
		String snapshot = diagram.toJSON();

		if (backStack.empty() || !snapshot.equals(backStack.peek())) {
			backStack.add(snapshot);
		}

		Log.d("touch", "snapshot added to backstack. size " + backStack.size());
	}

	private boolean onPaintTouch(View v, MotionEvent event) {
		Touch currTouch = getTouch(event);
		Point2D_F32 touchPoint = currTouch.getCoords();
		LineSegment2D_F32 closestLine = getClosestLine(touchPoint, null);

		if (closestLine != null) {
			float distance = Distance2D_F32.distance(closestLine, touchPoint);
			boolean touchOnLine = distance < MAX_POINT_LINE_DIST;

			if (touchOnLine) {
				diagram.setColor(closestLine, drawingColor);
			}
		}

		addSnapshotToBackstack();

		return true;
	}

	private boolean onDrawTouch(View v, MotionEvent event) {
		Touch currTouch = getTouch(event);

		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			gestureStart = new Touch();
			gestureStart.set(currTouch);

			gestureEnd = new Touch();
			gestureEnd.set(currTouch);

			dragMode = false;
		} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
			long currentTime = System.currentTimeMillis();

			if (gestureStart != null) {
				gestureEnd.set(currTouch);

				// check if we have to enter in drag mode
				if (!dragMode) {
					Point2D_F32 a = gestureStart.getCoords();
					Point2D_F32 b = gestureEnd.getCoords();
					boolean notMoved = a.isIdentical(b, MAX_DELTA_FOR_POINT);
					boolean isLongClick = currentTime - gestureStart.getTime() > LONG_CLICK_TIME_MS;
					if (notMoved && isLongClick
							&& currTouch.isOnExistingPoint()) {
						dragMode = true;
						draggedPoint = currTouch.getCoords();

						draggedLines = new ArrayList<LineSegment2D_F32>();
						for (LineSegment2D_F32 line : diagram.getLines()) {
							boolean identicalA = line.getA().isIdentical(
									draggedPoint, EPSILON);
							boolean identicalB = line.getB().isIdentical(
									draggedPoint, EPSILON);
							if (identicalA || identicalB) {
								draggedLines.add(line);
							}
						}

						Log.d("Dragged lines: ", "" + draggedLines.size());

						// notify the changed mode
						shortVibration();

						diagram.deletePointOnLineDefinition(draggedPoint);
					}
				} else { // if in drag mode
					gestureStart.set(currTouch);
					gestureEnd.set(currTouch);
					draggedPoint.set(currTouch.getCoords());
					refreshPointOnLineDefinitions();
				}
			}
		} else if (event.getAction() == MotionEvent.ACTION_UP) {
			if (!dragMode) {
				Point2D_F32 a = gestureStart.getCoords();
				Point2D_F32 b = gestureEnd.getCoords();

				boolean thatIsAPoint = a.isIdentical(b, MAX_DELTA_FOR_POINT);

				if (thatIsAPoint) {
					Point2D_F32 point = gestureEnd.getCoords();

					diagram.addPoint(point);
				} else {
					LineSegment2D_F32 newLine = new LineSegment2D_F32();
					newLine.a = a;
					newLine.b = b;
					diagram.addLine(newLine);
					diagram.setColor(newLine, drawingColor);

					addPoint(a, diagram);
					addPoint(b, diagram);
				}
			}

			if (gestureEnd.isOnLine()) {
				PointOnLineDefinition definition = gestureEnd.getDefinition();
				if (dragMode) {
					definition.setPoint(draggedPoint);
				}
				diagram.addPointOnLineDefinition(definition);
			}

			gestureStart = null;
			gestureEnd = null;
			dragMode = false;
			draggedLines = null;
			draggedPoint = null;

			addSnapshotToBackstack();
		}

		return true;
	}

	private Touch getTouch(MotionEvent event) {
		long time = System.currentTimeMillis();

		Touch currTouch = new Touch();

		float x = event.getX();
		float y = event.getY();

		Point2D_F32 touchPoint = new Point2D_F32((int) x, (int) y);

		boolean touchOnExistingPoint = false;
		boolean touchOnLine = false;
		LineSegment2D_F32 closestLine = null;

		Point2D_F32 closestPointToTouch;
		if (!dragMode) {
			closestPointToTouch = getClosestPoint(touchPoint, null);
		} else {
			closestPointToTouch = getClosestPoint(touchPoint, draggedPoint);
		}

		touchOnExistingPoint = closestPointToTouch != null;

		if (touchOnExistingPoint) {
			touchPoint = closestPointToTouch;
		} else {
			// set the point on the closest line
			closestLine = getClosestLine(touchPoint, draggedLines);
			boolean closestFound = closestLine != null;

			if (closestFound) {
				float distance = Distance2D_F32.distance(closestLine,
						touchPoint);
				touchOnLine = distance < MAX_POINT_LINE_DIST;
				if (touchOnLine) {
					touchPoint = GDUtil.project(touchPoint, closestLine);

					float k = (touchPoint.x - closestLine.a.x)
							/ (closestLine.b.x - closestLine.a.x);

					PointOnLineDefinition definition = new PointOnLineDefinition(
							touchPoint, closestLine, k);

					currTouch.setDefinition(definition);
				}
			}
		}

		currTouch.setCoords(touchPoint);
		currTouch.setOnExistingPoint(touchOnExistingPoint);
		currTouch.setOnLine(touchOnLine);
		currTouch.setLine(closestLine);
		currTouch.setTime(time);

		return currTouch;
	}

	private void shortVibration() {
		Vibrator vibrator = (Vibrator) context
				.getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(100);
	}

	private void addPoint(Point2D_F32 point, Diagram diagram) {
		boolean contains = false;
		for (Point2D_F32 existingPoint : diagram.getPoints()) {
			if (point.isIdentical(existingPoint, EPSILON)) {
				contains = true;
				break;
			}
		}

		if (!contains) {
			diagram.addPoint(point);
		}
	}

	private void refreshPointOnLineDefinitions() {
		List<PointOnLineDefinition> pointsOnLines = diagram
				.getPointsOnLinesDefinitions();

		for (PointOnLineDefinition definition : pointsOnLines) {
			Point2D_F32 point = definition.getPoint();
			LineSegment2D_F32 line = definition.getLine();
			float k = definition.getK();

			point.x = k * (line.b.x - line.a.x) + line.a.x;
			point.y = k * (line.b.y - line.a.y) + line.a.y;
		}
	}

	private Point2D_F32 getClosestPoint(Point2D_F32 toThis,
			Point2D_F32 whichIsNot) {
		Point2D_F32 closest = null;
		float minDist = Float.MAX_VALUE;

		for (Point2D_F32 p : diagram.getPoints()) {
			float distance = toThis.distance2(p);
			boolean inRange = distance < MAX_POINT_CLICK_OFFSET
					* MAX_POINT_CLICK_OFFSET;

			boolean allowed = (whichIsNot != null) ? !p.isIdentical(whichIsNot,
					EPSILON) : true;
			if (allowed && inRange && (distance < minDist)) {
				minDist = distance;
				closest = p;
			}
		}

		return closest;
	}

	private LineSegment2D_F32 getClosestLine(Point2D_F32 toThis,
			List<LineSegment2D_F32> whichIsNot) {
		LineSegment2D_F32 best = null;
		float minDist = Float.MAX_VALUE;

		for (LineSegment2D_F32 line : diagram.getLines()) {
			float currDist = Distance2D_F32.distance(line, toThis);
			if (currDist != 0.0f && currDist < minDist) {
				boolean allowed = (whichIsNot != null) ? !whichIsNot
						.contains(line) : true;

				if (allowed) {
					minDist = currDist;
					best = line;
				}
			}
		}

		return best;
	}

}
