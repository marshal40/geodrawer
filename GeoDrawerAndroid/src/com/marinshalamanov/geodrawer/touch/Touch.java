package com.marinshalamanov.geodrawer.touch;

import georegression.struct.line.LineSegment2D_F32;
import georegression.struct.point.Point2D_F32;

import com.marinshalamanov.geodrawer.model.PointOnLineDefinition;

public class Touch {
	private Point2D_F32 coords;

	private boolean onExistingPoint;

	private boolean onLine;

	private LineSegment2D_F32 line;

	private long time;

	private PointOnLineDefinition definition;

	public Touch() {
		onExistingPoint = false;
		onLine = false;
		line = null;
	}

	public void set(Touch other) {
		coords = other.coords;
		onExistingPoint = other.onExistingPoint;
		onLine = other.onLine;
		line = other.line;
		time = other.time;
		definition = other.definition;
	}

	public Point2D_F32 getCoords() {
		return coords;
	}

	public void setCoords(Point2D_F32 coords) {
		this.coords = coords;
	}

	public boolean isOnExistingPoint() {
		return onExistingPoint;
	}

	public void setOnExistingPoint(boolean onExistingPoint) {
		this.onExistingPoint = onExistingPoint;
	}

	public boolean isOnLine() {
		return onLine;
	}

	public void setOnLine(boolean onLine) {
		this.onLine = onLine;
	}

	public LineSegment2D_F32 getLine() {
		return line;
	}

	public void setLine(LineSegment2D_F32 line) {
		this.line = line;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public PointOnLineDefinition getDefinition() {
		return definition;
	}

	public void setDefinition(PointOnLineDefinition definition) {
		this.definition = definition;
	}
}
