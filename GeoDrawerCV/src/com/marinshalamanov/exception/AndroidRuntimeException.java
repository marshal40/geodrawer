package com.marinshalamanov.exception;

public class AndroidRuntimeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7833162075591624783L;

	public AndroidRuntimeException(String message) {
		super(message);
	}

	public AndroidRuntimeException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
