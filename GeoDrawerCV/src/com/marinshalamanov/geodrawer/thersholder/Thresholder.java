package com.marinshalamanov.geodrawer.thersholder;

import boofcv.struct.image.ImageFloat32;
import boofcv.struct.image.ImageUInt8;

public interface Thresholder {

	public ImageUInt8 threshold(ImageFloat32 image, ImageUInt8 thresholded);
}
