package com.marinshalamanov.geodrawer.thersholder;

import boofcv.alg.feature.detect.grid.HistogramTwoPeaks;
import boofcv.alg.feature.detect.grid.IntensityHistogram;
import boofcv.alg.filter.binary.ThresholdImageOps;
import boofcv.struct.image.ImageFloat32;
import boofcv.struct.image.ImageUInt8;

public class HistogramThresholder implements Thresholder {

	@Override
	public ImageUInt8 threshold(ImageFloat32 image, ImageUInt8 thresholded) {
		if (thresholded == null) {
			thresholded = new ImageUInt8(image.getWidth(), image.getHeight());
		}

		double maxIntensity = Double.MIN_VALUE;
		for (double currIntensity : image.data) {
			maxIntensity = Math.max(maxIntensity, currIntensity);
		}

		IntensityHistogram histogram = new IntensityHistogram((int) maxIntensity, maxIntensity + 1);

		histogram.add(image);

		HistogramTwoPeaks twoPeaks = new HistogramTwoPeaks(30);
		twoPeaks.computePeaks(histogram);
		double peakHigh = twoPeaks.peakHigh;
		double peakLow = twoPeaks.peakLow;

		float threshold = (float) ((peakHigh + peakLow) / 2.0f);
		ThresholdImageOps.threshold(image, thresholded, threshold, false);

		return thresholded;
	}

}
