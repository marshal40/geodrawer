package com.marinshalamanov.geodrawer.thersholder;

import java.awt.image.BufferedImage;

import boofcv.alg.misc.ImageStatistics;
import boofcv.gui.binary.VisualizeBinaryData;
import boofcv.gui.image.ShowImages;
import boofcv.struct.image.ImageFloat32;
import boofcv.struct.image.ImageUInt8;

import com.marinshalamanov.geodrawer.GDUtil;

public class SectionThresholder implements Thresholder {

	private int numHorizontalSections;

	private int numVerticalSection;

	private ImageUInt8 thresholded;

	private ImageFloat32 image;

	// marks which sections are empty, i.e. no parts of the diagram on them
	private boolean[][] empty;

	private double sectionWidth;

	private double sectionHeight;

	public SectionThresholder() {
		numHorizontalSections = 10;
		numVerticalSection = 10;
	}

	public SectionThresholder(int aNumHorizontalSections, int aNumVerticalSection) {
		numHorizontalSections = aNumHorizontalSections;
		numVerticalSection = aNumVerticalSection;
	}

	@Override
	public ImageUInt8 threshold(ImageFloat32 image, ImageUInt8 outThresholded) {
		if (outThresholded != null) {
			throw new IllegalStateException("Not supported.");
		}

		this.image = image;
		thresholded = new ImageUInt8(image.getWidth(), image.getHeight());

		thresholdSections();

		// displayThresholded();

		removeEmptyCornerSections();

		// displayThresholded();

		return thresholded;
	}

	private void thresholdSections() {
		sectionWidth = image.getWidth() / (double) numHorizontalSections;
		sectionHeight = image.getHeight() / (double) numVerticalSection;

		empty = new boolean[numHorizontalSections][numVerticalSection];

		for (int i = 0; i < numHorizontalSections; i++) {
			for (int j = 0; j < numVerticalSection; j++) {
				int x0 = (int) (i * sectionWidth);
				int y0 = (int) (j * sectionHeight);
				int x1 = (int) ((i + 1) * sectionWidth);
				int y1 = (int) ((j + 1) * sectionHeight);

				ImageFloat32 subimage = image.subimage(x0, y0, x1, y1);
				ImageUInt8 subThresholded = thresholded.subimage(x0, y0, x1, y1);

				HistogramThresholder thresholder = new HistogramThresholder();
				thresholder.threshold(subimage, subThresholded);

				// check if the image is black
				double meanThresholded = ImageStatistics.mean(subThresholded);
				empty[i][j] = meanThresholded < 0.65;
				if (empty[i][j]) {
					GDUtil.fill(subThresholded, (byte) 1);
				}
			}
		}

		// System.out.println("Empty cells:");
		// for (int i = 0; i < numHorizontalSections; i++) {
		// for (int j = 0; j < numVerticalSection; j++) {
		// System.out.print((empty[i][j] ? 1 : 0) + " ");
		// }
		// System.out.println();
		// }
	}

	/**
	 * Suppose the empty array looks like: </br> 0011111 </br> 1111111 </br>
	 * 1100011 </br> 1100011 </br> 1100111 </br> 1111111 </br> where, the center
	 * sections of zeros is the diagram. We can conclude that the top left two
	 * unempty cells are actually empty.
	 * <p>
	 * This function aims to locate the diagram sections (with BFS), and erase
	 * all other sections.
	 */
	private void removeEmptyCornerSections() {
		// TODO: locate all unempty regions and consider the biggest one as
		// diagram container

		// remove the unempty cells at the corners
		int[][] regionCode = new int[numHorizontalSections][numVerticalSection];

		int currRegionCode = 0;
		// the size of the array is bigger than regular case for safety
		int[] numCellsInRegion = new int[numHorizontalSections * numVerticalSection];

		// BFS on the unempty cells
		for (int reachedX = 0; reachedX < numHorizontalSections; reachedX++) {
			for (int reachedY = 0; reachedY < numVerticalSection; reachedY++) {
				if (!empty[reachedX][reachedY] && regionCode[reachedX][reachedY] == 0) {
					currRegionCode++;

					// the size of the queue is bigger than needed, just to be
					// sure
					int[][] queue = new int[5 * numHorizontalSections * numVerticalSection][2];
					int queueEnd = 0;
					queue[queueEnd++] = new int[] { reachedX, reachedY };

					for (int queueIndex = 0; queueIndex < queueEnd; queueIndex++) {
						int x = queue[queueIndex][0];
						int y = queue[queueIndex][1];
						boolean invalidX = x < 0 || x >= numHorizontalSections;
						boolean invalidY = y < 0 || y >= numVerticalSection;
						if (invalidX || invalidY) {
							continue;
						}
						if (regionCode[x][y] != 0) {
							continue;
						}
						if (empty[x][y]) { // avoid empty cells
							continue;
						}

						regionCode[x][y] = currRegionCode;
						numCellsInRegion[currRegionCode]++;

						queue[queueEnd++] = new int[] { x, y + 1 };
						queue[queueEnd++] = new int[] { x, y - 1 };
						queue[queueEnd++] = new int[] { x + 1, y };
						queue[queueEnd++] = new int[] { x - 1, y };
					}
				}
			}
		}

		int largestRegionIndex = 1; // default value
		int largestRegionSize = -1;
		for (int i = 0; i <= currRegionCode; i++) {
			if (numCellsInRegion[i] > largestRegionSize) {
				largestRegionSize = numCellsInRegion[i];
				largestRegionIndex = i;
			}
		}

		// System.out.println("Region codes:");
		// for (int i = 0; i < numHorizontalSections; i++) {
		// for (int j = 0; j < numVerticalSection; j++) {
		// System.out.print(regionCode[i][j] + " ");
		// }
		// System.out.println();
		// }

		for (int i = 0; i < numHorizontalSections; i++) {
			for (int j = 0; j < numVerticalSection; j++) {
				if (regionCode[i][j] != largestRegionIndex && !empty[i][j]) {
					int x0 = (int) (i * sectionWidth);
					int y0 = (int) (j * sectionHeight);
					int x1 = (int) ((i + 1) * sectionWidth);
					int y1 = (int) ((j + 1) * sectionHeight);

					ImageUInt8 subThresholded = thresholded.subimage(x0, y0, x1, y1);
					GDUtil.fill(subThresholded, (byte) 1);
				}
			}
		}
	}

	@SuppressWarnings("unused")
	private void displayThresholded() {
		// if android do nothing
		if (GDUtil.isAndroid()) {
			return;
		}
		BufferedImage thresholdedBuffered = VisualizeBinaryData.renderBinary(thresholded, null);
		ShowImages.showWindow(thresholdedBuffered, "Binary Original");
	}

}
