package com.marinshalamanov.geodrawer;

import georegression.metric.Intersection2D_F32;
import georegression.struct.line.LineParametric2D_F32;
import georegression.struct.line.LineSegment2D_F32;
import georegression.struct.point.Point2D_F32;
import georegression.struct.point.Vector2D_F32;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Properties;

import boofcv.core.image.ConvertBufferedImage;
import boofcv.gui.feature.ImageLinePanel;
import boofcv.gui.image.ShowImages;
import boofcv.struct.image.ImageUInt8;

public class GDUtil {

	private static final float EPSILON = (float) 1e-5;

	/**
	 * 
	 * @return if the curent runtime is an android device
	 */
	public static boolean isAndroid() {
		Properties properties = System.getProperties();
		boolean vmVendor = properties.getProperty("java.vm.vendor").equals("The Android Project");
		boolean vendor = properties.getProperty("java.vendor").equals("The Android Project");
		boolean androidRuntime = properties.getProperty("java.runtime.name").equals(
				"Android Runtime");

		return vmVendor || vendor || androidRuntime;
	}

	public static void displayImage(BufferedImage thresholded, String title) {
		if (GDUtil.isAndroid()) {
			return;
		} else {
			displayImageImpl(thresholded, title);
		}
	}

	private static void displayImageImpl(BufferedImage thresholded, String title) {
		ImageLinePanel gui = new ImageLinePanel();
		gui.setBackground(thresholded);
		gui.setPreferredSize(new Dimension(thresholded.getWidth(), thresholded.getHeight()));
		ShowImages.showWindow(gui, title);
	}

	public static void fill(ImageUInt8 image, byte intensity) {
		for (int y = 0; y < image.height; y++) {

			int index = image.startIndex + y * image.stride;
			int end = index + image.width;

			for (; index < end; index++) {
				image.data[index] = intensity;
			}
		}
	}

	public static void displayLines(ImageUInt8 image, List<LineParametric2D_F32> lines, String title) {
		if (GDUtil.isAndroid()) {
			return;
		}

		ImageLinePanel gui = new ImageLinePanel();
		BufferedImage thresholdedBuffered = ConvertBufferedImage.convertTo(image, null);
		gui.setBackground(thresholdedBuffered);
		gui.setLines(lines);
		gui.setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
		ShowImages.showWindow(gui, title);
	}

	public static Point2D_F32 project(Point2D_F32 point, LineSegment2D_F32 line) {
		float slopeX, slopeY;

		slopeX = -line.slopeY();
		slopeY = line.slopeX();

		Vector2D_F32 slopePerpendicular = new Vector2D_F32(slopeX, slopeY);
		LineParametric2D_F32 linePerpendicular = new LineParametric2D_F32(point, slopePerpendicular);
		float intersection = Intersection2D_F32.intersection(linePerpendicular, line);

		Point2D_F32 projection = linePerpendicular.getPointOnLine(intersection);

		return projection;
	}

	public static boolean areIdentical(LineSegment2D_F32 line1, LineSegment2D_F32 line2) {
		boolean exact = line1.a.isIdentical(line2.a, EPSILON)
				&& line1.b.isIdentical(line2.b, EPSILON);
		boolean reverse = line1.a.isIdentical(line2.b, EPSILON)
				&& line1.b.isIdentical(line2.a, EPSILON);

		return exact || reverse;
	}

}
