package com.marinshalamanov.geodrawer.model;

import georegression.struct.line.LineSegment2D_F32;
import georegression.struct.point.Point2D_F32;
import georegression.struct.shapes.Rectangle2D_F32;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import boofcv.gui.image.ShowImages;

import com.google.gson.Gson;
import com.marinshalamanov.geodrawer.GDUtil;

public class Diagram {

	private static final float EPSILON = (float) 1e-2;

	private static final int BLACK = 0xff000000;

	private List<Point2D_F32> points;

	private List<LineSegment2D_F32> lines;

	private List<Integer> colors; // same indexes as @lines

	private List<PointOnLineDefinition> pointsOnLines;

	public Diagram() {
		points = new ArrayList<Point2D_F32>();
		lines = new ArrayList<LineSegment2D_F32>();
		colors = new ArrayList<Integer>();
		pointsOnLines = new ArrayList<PointOnLineDefinition>();
	}

	public Diagram(List<Point2D_F32> points, List<LineSegment2D_F32> lines) {
		this.points = points;
		this.lines = lines;
		colors = new ArrayList<Integer>();
		pointsOnLines = new ArrayList<PointOnLineDefinition>();
	}

	/**
	 * Leaves the diagram empty;
	 */
	public void clear() {
		lines.clear();
		points.clear();
		colors.clear();
		pointsOnLines.clear();
	}

	public void addData(Diagram diagram) {
		if (diagram.points != null) {
			points.addAll(diagram.points);
		}

		if (diagram.lines != null) {
			lines.addAll(diagram.lines);
		}

		if (diagram.colors != null) {
			colors.addAll(diagram.colors);
		}

		if (diagram.pointsOnLines != null) {
			pointsOnLines.addAll(diagram.pointsOnLines);
		}
	}

	public void set(Diagram diagram) {
		clear();
		addData(diagram);
	}

	public void addPoint(Point2D_F32 point) {
		points.add(point);
	}

	public void addLine(LineSegment2D_F32 line) {
		lines.add(line);
	}

	public List<Point2D_F32> getPoints() {
		return points;
	}

	public List<LineSegment2D_F32> getLines() {
		return lines;
	}

	/**
	 * Sets the color of a the line given.
	 * 
	 * @param line
	 * @param color
	 *            - The color should be provided as a hexcode (0xAARRGGBB).
	 */
	public void setColor(LineSegment2D_F32 line, int color) {
		ensureColorsLinesAreEqual();
		for (int i = 0; i < lines.size(); i++) {
			if (GDUtil.areIdentical(line, lines.get(i))) {
				colors.set(i, color);
			}
		}
		// lineColors.put(line, color);
	}

	/**
	 * Returns the color of the line as a hexcode (0xAARRGGBB). If no color is
	 * specified black (0xff000000) is returned.
	 */
	public int getColor(LineSegment2D_F32 line) {
		ensureColorsLinesAreEqual();
		for (int i = 0; i < lines.size(); i++) {
			if (GDUtil.areIdentical(line, lines.get(i))) {
				int color = colors.get(i);
				return color;
			}
		}

		return BLACK; // default
	}

	private void ensureColorsLinesAreEqual() {
		for (int i = colors.size(); i < lines.size(); i++) {
			colors.add(BLACK);
		}
	}

	/**
	 * Deletes the point-on-line definition for a point if such exists
	 * 
	 * @param point
	 */
	public void deletePointOnLineDefinition(Point2D_F32 point) {
		for (int i = 0; i < pointsOnLines.size(); i++) {
			PointOnLineDefinition definition = pointsOnLines.get(i);
			if (definition.getPoint().isIdentical(point, EPSILON)) {
				pointsOnLines.remove(i);
				i--;
			}
		}
	}

	public void addPointOnLineDefinition(PointOnLineDefinition definition) {
		pointsOnLines.add(definition);
	}

	public List<PointOnLineDefinition> getPointsOnLinesDefinitions() {
		return pointsOnLines;
	}

	public void display(BufferedImage image, String title) {
		// if android do nothing
		if (GDUtil.isAndroid()) {
			return;
		}

		Graphics2D g2 = image.createGraphics();

		g2.setStroke(new BasicStroke(3));

		for (LineSegment2D_F32 line : lines) {
			int currColorHex = getColor(line);
			Color currColor = new Color(currColorHex);
			g2.setColor(currColor);
			g2.drawLine((int) line.a.x, (int) line.a.y, (int) line.b.x, (int) line.b.y);
		}

		Color pointsColor = Color.RED;
		g2.setColor(pointsColor);
		int pointRad = 14;
		for (Point2D_F32 point : points) {
			g2.drawOval((int) point.x - pointRad / 2, (int) point.y - pointRad / 2, pointRad,
					pointRad);
		}

		ShowImages.showWindow(image, title);
	}

	/**
	 * Multiplies each point's coordinates by <i>scale</i>
	 * 
	 * @param scale
	 */
	public void scale(float scale) {
		for (Point2D_F32 point : points) {
			point.x *= scale;
			point.y *= scale;
		}
	}

	public void shrink(int width, int height) {
		Rectangle2D_F32 bounds = getBounds();

		float coefWidth = width / bounds.width;
		float coefHeight = height / bounds.height;
		float coef = Math.min(coefWidth, coefHeight);

		for (Point2D_F32 point : points) {
			point.x -= bounds.tl_x;
			point.y -= bounds.tl_y;

			point.x *= coef;
			point.y *= coef;
		}
	}

	public void offset(int x, int y) {
		for (Point2D_F32 point : points) {
			point.x += x;
			point.y += y;
		}
	}

	public Rectangle2D_F32 getBounds() {
		float topLeftX = Float.MAX_VALUE;
		float topLeftY = Float.MAX_VALUE;
		float bottomRightX = Float.MIN_VALUE;
		float bottomRightY = Float.MIN_VALUE;

		for (Point2D_F32 point : points) {
			if (point.x < topLeftX) {
				topLeftX = point.x;
			}
			if (point.y < topLeftY) {
				topLeftY = point.y;
			}
			if (point.x > bottomRightX) {
				bottomRightX = point.x;
			}
			if (point.y > bottomRightY) {
				bottomRightY = point.y;
			}
		}

		float width = bottomRightX - topLeftX;
		float height = bottomRightY - topLeftY;
		Rectangle2D_F32 bounds = new Rectangle2D_F32(topLeftX, topLeftY, width, height);
		return bounds;
	}

	public String toJSON() {
		Gson gson = new Gson();
		String json = gson.toJson(this);
		return json;
	}

	public static Diagram createFromJSON(String json) {
		Gson gson = new Gson();
		Diagram result = gson.fromJson(json, Diagram.class);

		for (LineSegment2D_F32 line : result.lines) {
			for (Point2D_F32 point : result.points) {
				if (point.isIdentical(line.a, 1)) {
					line.a = point;
				}
				if (point.isIdentical(line.b, 1)) {
					line.b = point;
				}
			}
		}

		for (PointOnLineDefinition definition : result.pointsOnLines) {
			Point2D_F32 defPoint = definition.getPoint();
			for (Point2D_F32 point : result.getPoints()) {
				if (point.isIdentical(defPoint, EPSILON)) {
					definition.setPoint(point);
					break;
				}
			}

			LineSegment2D_F32 defLine = definition.getLine();
			for (LineSegment2D_F32 line : result.getLines()) {
				if (GDUtil.areIdentical(line, defLine)) {
					definition.setLine(line);
					break;
				}
			}
		}

		return result;
	}
}
