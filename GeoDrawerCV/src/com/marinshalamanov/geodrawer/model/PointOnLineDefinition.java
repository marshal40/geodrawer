package com.marinshalamanov.geodrawer.model;

import georegression.struct.line.LineSegment2D_F32;
import georegression.struct.point.Point2D_F32;

public class PointOnLineDefinition {

	private Point2D_F32 point;

	private LineSegment2D_F32 line;

	private float k;

	public PointOnLineDefinition(Point2D_F32 point, LineSegment2D_F32 line, float k) {
		super();
		this.point = point;
		this.line = line;
		this.k = k;
	}

	public Point2D_F32 getPoint() {
		return point;
	}

	public void setPoint(Point2D_F32 point) {
		this.point = point;
	}

	public LineSegment2D_F32 getLine() {
		return line;
	}

	public void setLine(LineSegment2D_F32 line) {
		this.line = line;
	}

	public float getK() {
		return k;
	}

	public void setK(float k) {
		this.k = k;
	}

}
