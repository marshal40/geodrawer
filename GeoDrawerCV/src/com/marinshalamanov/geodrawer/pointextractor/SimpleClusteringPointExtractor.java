package com.marinshalamanov.geodrawer.pointextractor;

import georegression.metric.Distance2D_F32;
import georegression.metric.Intersection2D_F32;
import georegression.struct.line.LineParametric2D_F32;
import georegression.struct.line.LineSegment2D_F32;
import georegression.struct.point.Point2D_F32;
import georegression.struct.point.Vector2D_F32;

import java.util.ArrayList;
import java.util.List;

public class SimpleClusteringPointExtractor implements PointExtractor {

	private static final float EPSILON = 2.0f;

	private int minPointDistance = 25; // default value

	private int minPointLineDistance = minPointDistance; // default value

	public void setImageDimensions(int width, int height) {
		int avg = (width + height) / 2;
		minPointDistance = avg / 15;
		minPointLineDistance = minPointDistance;
	}

	@Override
	public List<Point2D_F32> extractPoints(List<LineSegment2D_F32> segments) {
		List<Point2D_F32> points;

		points = collectThePoints(segments);

		mergeClosePoints(points);

		uniquePoints(points);

		movePointsOnCloseLines(points, segments);

		fixSegmentsEnds(points, segments);

		return points;
	}

	private List<Point2D_F32> collectThePoints(List<LineSegment2D_F32> segments) {
		List<Point2D_F32> points = new ArrayList<Point2D_F32>();

		// make a collection of all points
		for (LineSegment2D_F32 segment : segments) {
			if (!points.contains(segment.a)) {
				points.add(segment.a);
			}

			if (!points.contains(segment.b)) {
				points.add(segment.b);
			}
		}

		return points;
	}

	private void mergeClosePoints(List<Point2D_F32> points) {
		// now some of points may be really close to each other
		// and they would be considered as different, although their the same
		// point
		for (Point2D_F32 point : points) {
			List<Point2D_F32> inCluster = new ArrayList<Point2D_F32>();
			inCluster.add(point);

			for (Point2D_F32 otherPoint : points) {
				if (point.isIdentical(otherPoint, EPSILON)) {
					continue;
				}

				float distance2 = point.distance2(otherPoint);
				if (distance2 < minPointDistance * minPointDistance) {
					inCluster.add(otherPoint);
				}
			}

			if (inCluster.size() > 1) {
				float avgX = 0;
				float avgY = 0;

				for (Point2D_F32 inClusterPoint : inCluster) {
					avgX += inClusterPoint.x;
					avgY += inClusterPoint.y;
				}

				avgX /= inCluster.size();
				avgY /= inCluster.size();

				Point2D_F32 clusterCenter = new Point2D_F32(avgX, avgY);

				// move each point of the cluster in the center
				for (Point2D_F32 inClusterPoint : inCluster) {
					inClusterPoint.set(clusterCenter);
				}
			}
		}
	}

	private void uniquePoints(List<Point2D_F32> points) {
		// unique the points set
		List<Point2D_F32> pointsUnique = new ArrayList<Point2D_F32>();
		for (Point2D_F32 point : points) {
			boolean exist = false;
			for (Point2D_F32 existingPoint : pointsUnique) {
				if (point.isIdentical(existingPoint, EPSILON)) {
					exist = true;
					break;
				}
			}
			if (!exist) {
				pointsUnique.add(point);
			}
		}

		points.clear();
		points.addAll(pointsUnique);
	}

	private void movePointsOnCloseLines(List<Point2D_F32> points, List<LineSegment2D_F32> segments) {
		// if a point is really close to a line but not exactly on the line,
		// than move it on the line

		for (Point2D_F32 point : points) {
			LineSegment2D_F32 closestSegment = null;
			float closestSegmentDist = Float.MAX_VALUE;

			for (LineSegment2D_F32 segment : segments) {
				boolean isBegin = segment.a.isIdentical(point, EPSILON);
				boolean isEnd = segment.b.isIdentical(point, EPSILON);

				if (isBegin || isEnd) {
					continue;
				}

				float distance = Distance2D_F32.distance(segment, point);
				if (distance < closestSegmentDist) {
					closestSegmentDist = distance;
					closestSegment = segment;
				}
			}

			if (closestSegmentDist <= minPointLineDistance) {
				float slopeX, slopeY;

				slopeX = -closestSegment.slopeY();
				slopeY = closestSegment.slopeX();

				Vector2D_F32 slopePerpendicular = new Vector2D_F32(slopeX, slopeY);
				LineParametric2D_F32 linePerpendicular = new LineParametric2D_F32(point,
						slopePerpendicular);
				float intersection = Intersection2D_F32.intersection(linePerpendicular,
						closestSegment);
				if (!Float.isNaN(intersection)) {
					Point2D_F32 projection = linePerpendicular.getPointOnLine(intersection);
					point.set(projection);
				}
			}
		}
	}

	/**
	 * After the unitement of the points is possible that some segment have the
	 * same endings as a coordinates but different objects. This method fixed
	 * the problem.
	 * 
	 * @param points
	 * @param segments
	 */
	private void fixSegmentsEnds(List<Point2D_F32> points, List<LineSegment2D_F32> segments) {
		for (LineSegment2D_F32 segment : segments) {
			for (Point2D_F32 point : points) {
				if (point.isIdentical(segment.a, EPSILON)) {
					segment.a = point;
				}

				if (point.isIdentical(segment.b, EPSILON)) {
					segment.b = point;
				}
			}
		}
	}
}
