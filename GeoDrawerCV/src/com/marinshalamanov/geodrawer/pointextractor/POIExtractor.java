package com.marinshalamanov.geodrawer.pointextractor;

import georegression.struct.point.Point2D_F32;
import georegression.struct.point.Point2D_F64;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import boofcv.abst.feature.detect.interest.ConfigFastHessian;
import boofcv.abst.feature.detect.interest.InterestPointDetector;
import boofcv.core.image.ConvertBufferedImage;
import boofcv.factory.feature.detect.interest.FactoryInterestPoint;
import boofcv.gui.feature.FancyInterestPointRender;
import boofcv.gui.image.ShowImages;
import boofcv.struct.BoofDefaults;
import boofcv.struct.image.ImageFloat32;

import com.marinshalamanov.exception.AndroidRuntimeException;
import com.marinshalamanov.geodrawer.GDUtil;

/**
 * Not really used.
 * 
 * TODO: This class should implement the {@link PointExtractor} interface.
 * 
 * @author Shalamanov
 */
public class POIExtractor {

	// TODO: bad idea; better be local
	private InterestPointDetector<ImageFloat32> detector;

	public List<Point2D_F32> extractPoints(BufferedImage image) {
		if (GDUtil.isAndroid()) {
			throw new AndroidRuntimeException("No able to execute this code on Android.");
		}

		ImageFloat32 input = ConvertBufferedImage
				.convertFromSingle(image, null, ImageFloat32.class);

		List<Point2D_F32> points = extractPoints(input);

		// Show the features
		displayPOIResults(image, detector);

		return points;
	}

	public List<Point2D_F32> extractPoints(ImageFloat32 input) {
		// Create a Fast Hessian detector from the SURF paper.
		// Other detectors can be used in this example too.
		detector = FactoryInterestPoint.fastHessian(new ConfigFastHessian(10, 2, 100, 2, 9, 3, 4));

		// find interest points in the image
		detector.detect(input);

		List<Point2D_F32> points = new ArrayList<Point2D_F32>();
		for (int i = 0; i < detector.getNumberOfFeatures(); i++) {
			Point2D_F64 point = detector.getLocation(i);
			// detector.getScale(i); can be used in order to get the magnitude
			// of interest
			Point2D_F32 newPoint = new Point2D_F32((float) point.x, (float) point.y);
			points.add(newPoint);
		}

		return points;
	}

	@SuppressWarnings("deprecation")
	private void displayPOIResults(BufferedImage image, InterestPointDetector<ImageFloat32> detector) {
		// if android omit displaying
		if (GDUtil.isAndroid()) {
			return;
		}

		Graphics2D g2 = image.createGraphics();
		FancyInterestPointRender render = new FancyInterestPointRender();

		for (int i = 0; i < detector.getNumberOfFeatures(); i++) {
			Point2D_F64 pt = detector.getLocation(i);

			// note how it checks the capabilities of the detector
			if (detector.hasScale()) {
				double scale = detector.getScale(i);
				int radius = (int) (scale * BoofDefaults.SCALE_SPACE_CANONICAL_RADIUS);
				render.addCircle((int) pt.x, (int) pt.y, radius);
			} else {
				render.addPoint((int) pt.x, (int) pt.y);
			}
		}
		// make the circle's thicker
		g2.setStroke(new BasicStroke(3));

		// just draw the features onto the input image
		render.draw(g2);
		ShowImages.showWindow(image, "Detected Points");
	}
}
