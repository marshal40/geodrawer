package com.marinshalamanov.geodrawer.pointextractor;

import georegression.struct.line.LineSegment2D_F32;
import georegression.struct.point.Point2D_F32;

import java.util.List;

public interface PointExtractor {

	public List<Point2D_F32> extractPoints(List<LineSegment2D_F32> segments);

	public void setImageDimensions(int width, int height);
}
