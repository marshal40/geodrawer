package com.marinshalamanov.geodrawer.segmentextractor;

import georegression.metric.Distance2D_F32;
import georegression.metric.Intersection2D_F32;
import georegression.struct.line.LineParametric2D_F32;
import georegression.struct.line.LineSegment2D_F32;
import georegression.struct.point.Point2D_F32;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import boofcv.abst.feature.detect.line.DetectLineHoughPolar;
import boofcv.factory.feature.detect.line.FactoryDetectLineAlgs;
import boofcv.struct.image.ImageSInt16;
import boofcv.struct.image.ImageUInt8;

public class SmartSegmentExtractor implements SegmentExtractor {

	private int maxPointLineDist = 25; // default value TODO: adjust to image
										// size

	private double minAngleBetweenLinesRad = 25 * Math.PI / 180.0;

	private ImageUInt8 image;

	private List<LineParametric2D_F32> lines;

	private int[] numBlackPoints;

	private Map<Double, Integer> angleCoeffs;

	public void setImageDimensions(int width, int height) {
		int avg = (width + height) / 2;
		maxPointLineDist = avg / 15;
	}

	@Override
	public List<LineSegment2D_F32> extractSegments(ImageUInt8 aImage) {
		image = aImage;

		lines = extractLines();
		// GDUtil.displayLines(image, lines, "Detected Lines");

		numBlackPoints = findNumBlackPoints(lines);

		angleCoeffs = sortByAngleCoeff(lines);
		lines = filterLines(lines);

		// GDUtil.displayLines(image, lines, "Filtered Lines");

		List<LineSegment2D_F32> segments = findSegments(lines);

		return segments;
	}

	private List<LineSegment2D_F32> findSegments(List<LineParametric2D_F32> lines) {
		List<LineSegment2D_F32> segments = new ArrayList<LineSegment2D_F32>(lines.size());

		for (int i = 0; i < lines.size(); i++) {
			LineParametric2D_F32 line = lines.get(i);

			float minIntersection = Float.MAX_VALUE;
			float maxIntersection = Float.MIN_VALUE;

			for (LineParametric2D_F32 otherLine : lines) {
				if (!line.equals(otherLine)) {
					float intersection = Intersection2D_F32.intersection(line, otherLine);
					Point2D_F32 intersectionPoint = line.getPointOnLine(intersection);
					boolean intersectionVisible = isPointVisible(intersectionPoint);

					if (intersectionVisible) {
						minIntersection = Math.min(minIntersection, intersection);
						maxIntersection = Math.max(maxIntersection, intersection);
					}
				}
			}

			if (minIntersection == Float.MAX_VALUE) {
				String message = "No minimal intersection found. " + lines.size() + " lines found";
				System.err.println(message);
				lines.remove(i);
				i--;
				continue;
				// throw new IllegalStateException(message);
			}

			if (maxIntersection == Float.MIN_VALUE) {
				String message = "No maximal intersection found. " + lines.size() + " lines found";
				System.err.println(message);
				// throw new IllegalStateException(message);
				lines.remove(i);
				i--;
				continue;
			}

			Point2D_F32 minIntersectionPoint = line.getPointOnLine(minIntersection);
			Point2D_F32 maxIntersectionPoint = line.getPointOnLine(maxIntersection);

			LineSegment2D_F32 segment = new LineSegment2D_F32(minIntersectionPoint,
					maxIntersectionPoint);

			segments.add(segment);
		}

		return segments;
	}

	private List<LineParametric2D_F32> filterLines(List<LineParametric2D_F32> lines) {
		// now the lines are sorted by their angle coefficient
		// we will check if two line has the same angle coefficient and they
		// do intersect in the visible area (they are not parallel) and in that
		// case the segment with less black points will be removed
		List<Map.Entry<Double, Integer>> angleCoeffsFiltered = new ArrayList<Map.Entry<Double, Integer>>();
		angleCoeffsFiltered.addAll(angleCoeffs.entrySet());

		for (int i = 0; i < angleCoeffsFiltered.size(); i++) {
			Map.Entry<Double, Integer> entry = angleCoeffsFiltered.get(i);

			for (int j = i - 1; j != i; j--) {
				if (j < 0) {
					j += angleCoeffsFiltered.size();
				}

				Map.Entry<Double, Integer> lastEntry = angleCoeffsFiltered.get(j);

				double deltaAngle = entry.getKey() - lastEntry.getKey();
				if (j > i) {
					deltaAngle += Math.PI;
				}

				if (deltaAngle < minAngleBetweenLinesRad) {

					int lastIndex = lastEntry.getValue();
					int currIndex = entry.getValue();
					LineParametric2D_F32 currLine = lines.get(currIndex);
					LineParametric2D_F32 lastLine = lines.get(lastIndex);
					float intersection = Intersection2D_F32.intersection(currLine, lastLine);

					Point2D_F32 intersectionPoint = currLine.getPointOnLine(intersection);

					boolean intersectionVisible = isPointVisible(intersectionPoint);
					if (intersectionVisible) {
						int currBlackPoints = numBlackPoints[currIndex];
						int lastBlackPoints = numBlackPoints[lastIndex];
						if (currBlackPoints > lastBlackPoints) {
							angleCoeffsFiltered.remove(j);
						} else {
							angleCoeffsFiltered.remove(i);
						}

						i = Math.max(i - 2, -1);
						break;
					}
				} else {
					break;
				}
			}
		}

		List<LineParametric2D_F32> linesFiltered = new ArrayList<LineParametric2D_F32>();
		for (Map.Entry<Double, Integer> entry : angleCoeffsFiltered) {
			Integer lineIndex = entry.getValue();
			LineParametric2D_F32 newLine = lines.get(lineIndex);
			linesFiltered.add(newLine);
		}
		return linesFiltered;
	}

	private boolean isPointVisible(Point2D_F32 point) {
		boolean xVisible = 0 <= point.x && point.x < image.width;

		boolean yVisible = 0 <= point.y && point.y < image.height;

		boolean pointVisible = xVisible && yVisible;
		return pointVisible;
	}

	private Map<Double, Integer> sortByAngleCoeff(List<LineParametric2D_F32> lines) {
		// angle coeff - index of the line map
		// this map is used for comparing the angles between the lines
		Map<Double, Integer> angleCoeffs = new TreeMap<Double, Integer>();

		for (int i = 0; i < lines.size(); i++) {
			LineParametric2D_F32 line = lines.get(i);
			float x = line.slope.x;
			float y = line.slope.y;
			if (y < 0) {
				y *= -1;
				x *= -1;
			}

			double angleCoeff = Math.atan2(y, x);

			if (!angleCoeffs.containsKey(angleCoeff)) {
				angleCoeffs.put(angleCoeff, i);
			} else {
				angleCoeff += 1e-5; // avoid overlapping entries
			}
		}
		return angleCoeffs;
	}

	private int[] findNumBlackPoints(List<LineParametric2D_F32> lines) {
		// find number of black points near each line
		int[] numBlackPoints = new int[lines.size()];
		for (int i = 0; i < lines.size(); i++) {
			LineParametric2D_F32 line = lines.get(i);

			for (int x = 0; x < image.getWidth(); x++) {
				for (int y = 0; y < image.getHeight(); y++) {
					if (image.get(x, y) == 0) { // if the point is black
						Point2D_F32 a = new Point2D_F32(x, y);
						float distance = Distance2D_F32.distance(line, a);
						if (distance < maxPointLineDist) {
							numBlackPoints[i]++; // may be weighted according to
													// the distance
						}
					}
				}
			}
		}
		return numBlackPoints;
	}

	private List<LineParametric2D_F32> extractLines() {
		// adjusts edge threshold for identifying pixels belonging to a line
		float edgeThreshold = 5;

		// adjust the maximum number of found lines in the image
		int maxLines = -1; // all lines
		double resolutionAngle = 0.5 * Math.PI / 180;
		int resolutionRange = 1;
		int minCounts = 30;
		int localMaxRadius = 3;

		DetectLineHoughPolar<ImageUInt8, ImageSInt16> detector = FactoryDetectLineAlgs.houghPolar(
				localMaxRadius, minCounts, resolutionRange, resolutionAngle, edgeThreshold,
				maxLines, ImageUInt8.class, ImageSInt16.class);

		List<LineParametric2D_F32> found = detector.detect(image);

		// ImageFloat32 transform = detector.getTransform().getTransform();
		// BufferedImage transformBuff =
		// ConvertBufferedImage.convertTo(transform, null);
		// GDUtil.displayImage(transformBuff, "Hough transform");

		return found;
	}
}