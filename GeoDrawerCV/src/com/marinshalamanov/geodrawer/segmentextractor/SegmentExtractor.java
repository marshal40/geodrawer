package com.marinshalamanov.geodrawer.segmentextractor;

import georegression.struct.line.LineSegment2D_F32;

import java.util.List;

import boofcv.struct.image.ImageUInt8;

public interface SegmentExtractor {

	public void setImageDimensions(int width, int height);

	public List<LineSegment2D_F32> extractSegments(ImageUInt8 image);
}
