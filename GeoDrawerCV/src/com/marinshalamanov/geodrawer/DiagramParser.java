package com.marinshalamanov.geodrawer;

import georegression.struct.line.LineSegment2D_F32;
import georegression.struct.point.Point2D_F32;

import java.awt.image.BufferedImage;
import java.util.List;

import boofcv.core.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.image.ImageFloat32;
import boofcv.struct.image.ImageUInt8;

import com.marinshalamanov.exception.AndroidRuntimeException;
import com.marinshalamanov.geodrawer.model.Diagram;
import com.marinshalamanov.geodrawer.pointextractor.PointExtractor;
import com.marinshalamanov.geodrawer.pointextractor.SimpleClusteringPointExtractor;
import com.marinshalamanov.geodrawer.segmentextractor.SegmentExtractor;
import com.marinshalamanov.geodrawer.segmentextractor.SmartSegmentExtractor;
import com.marinshalamanov.geodrawer.thersholder.SectionThresholder;
import com.marinshalamanov.geodrawer.thersholder.Thresholder;

public class DiagramParser {

	private Thresholder thresholder;

	private SegmentExtractor lineExtractor;

	private PointExtractor pointExtractor;

	public DiagramParser(Thresholder thresholder, SegmentExtractor lineExtractor,
			PointExtractor pointExtractor) {
		this.thresholder = thresholder;
		this.lineExtractor = lineExtractor;
		this.pointExtractor = pointExtractor;
	}

	public Diagram parse(String imgUrl) {
		BufferedImage imageBuffered = loadImage(imgUrl);

		ImageFloat32 imageF23 = ConvertBufferedImage.convertFromSingle(imageBuffered, null,
				ImageFloat32.class);

		Diagram diagram = parse(imageF23);
		return diagram;
	}

	public Diagram parse(ImageFloat32 imageF23) {
		ImageUInt8 thresholdedU8 = thresholder.threshold(imageF23, null);

		int imageHeight = imageF23.getHeight();
		int imageWidth = imageF23.getWidth();
		lineExtractor.setImageDimensions(imageWidth, imageHeight);
		List<LineSegment2D_F32> segments = lineExtractor.extractSegments(thresholdedU8);

		pointExtractor.setImageDimensions(imageWidth, imageHeight);
		List<Point2D_F32> points = pointExtractor.extractPoints(segments);

		Diagram diagram = bindDiagram(points, segments);
		return diagram;
	}

	private BufferedImage loadImage(String imgUrl) {
		if (GDUtil.isAndroid()) {
			throw new AndroidRuntimeException("No able to execute this code on Android.");
		}

		BufferedImage inputBuffered = UtilImageIO.loadImage(imgUrl);
		return inputBuffered;
	}

	private Diagram bindDiagram(List<Point2D_F32> points, List<LineSegment2D_F32> lines) {
		Diagram diagram = new Diagram(points, lines);
		// diagram.display(imageBuffered, "Diagram");
		return diagram;
	}

	public static void main(String[] args) {
		SectionThresholder thresholder = new SectionThresholder();
		SmartSegmentExtractor lineExtractor = new SmartSegmentExtractor();
		PointExtractor pointExtractor = new SimpleClusteringPointExtractor();
		DiagramParser diagramParser = new DiagramParser(thresholder, lineExtractor, pointExtractor);

		// args = new String[] { "data/img/3.jpg", "data/img/4.jpg",
		// "data/img/5.jpg",
		// "data/img/6.jpg" };

		if (args.length == 0) {
			String imgUrl = "data/img/6.jpg";
			Diagram diagram = diagramParser.parse(imgUrl);
			System.out.println(diagram.getLines().toString());
			System.out.println(diagram.getPoints().toString());

			BufferedImage imageBuffered = UtilImageIO.loadImage(imgUrl);
			diagram.display(imageBuffered, "Diagram");
		} else {
			for (String url : args) {
				diagramParser.parse(url);
			}
		}
	}
}
